var imageData="";



/*==Input file operation to detect the processs=====*/

$(function () {
    $(":file").change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var totalBytes = this.files[0].size
            var _size = Math.floor(totalBytes/1000);
             if(_size<500){
                  reader.onload = imageIsLoaded;
                }else{
                    alert("Image size uploaded should not exceed 2 MB")
                 }

             reader.readAsDataURL(this.files[0]);
        }
    });
});





/*===Load the image============*/

function imageIsLoaded(e) {
	  imageData = e.target.result
	  var Img = document.getElementById("myImg");
      Img.src = e.target.result
	
};
   



window.onload = function() {


  /*==check the local storage are we coming from edit button? ====*/

  var retrievedObject = localStorage.getItem('testObject');
    if(retrievedObject){

        /*==If id is available in the localstorage load that record====*/
    	getRecord(retrievedObject);
    }else{
    	console.log("no id the localStorage");
    }



  
   /*==creating the dropdown list for the age =====*/

    var select = document.getElementById('ageId'); 
        for(i=1; i<=100; i++) {
            option = document.createElement('option');
            option.setAttribute('value', i);
            option.innerHTML = i;
            select.appendChild(option);
        }

}





 function getRecord(id){

       var ajaxRequest = new XMLHttpRequest();
			if (ajaxRequest) {
			ajaxRequest.onreadystatechange = ajaxResponse;
			ajaxRequest.open("GET", "/api/user/"+id); // Where?
			ajaxRequest.send(null);
			}



            function ajaxResponse() {

                 if(ajaxRequest.readyState != 4){
                          console.log("Request not complete")
                 }else if(ajaxRequest.status == 200){
                        loadProfile(ajaxRequest.response);
                 }else{
                 	 alert("its in error, please try again")
                 }
    

          }
}


function loadProfile(profileObj){

        /*==convert string date to json to parse it easily====*/

	    var data = JSON.parse(profileObj)
        document.getElementById("name").value = data.name;
        document.getElementById("email").value = data.email;
        document.getElementById('ageId').value = data.age;
        document.getElementById('genderId').value = data.gender;
        var Img = document.getElementById("myImg");
        Img.src = data.profilepic
        imageData = data.profilepic;
}


/*====save the profile===============*/

function saveProfile(){


  if(document.getElementById("name").value!="" && document.getElementById("email").value!="" && imageData!=""){
 
    var retrievedObject = localStorage.getItem('testObject');
 
                if(retrievedObject){
                    saveRecord(retrievedObject);
                }else{
                    saveRecord(0)
                }

   } else{

      alert("Please complete all details")
   }

}

function saveRecord(id){

       var  parameter  =  "name="+document.getElementById("name").value+"&email="+document.getElementById("email").value+"&age="+document.getElementById('ageId').value+"&gender="+document.getElementById('genderId').value+"&profilepic="+encodeURIComponent(imageData);
       var ajaxRequest = new XMLHttpRequest();
            // Doesn’t work for IE6 and older
            if (ajaxRequest) {
            // if the object was created successfully
            ajaxRequest.onreadystatechange = ajaxResponse;

            /*==To save the profile making a single call changing url and methode type based upon the operation===*/

            if(id==0){
            ajaxRequest.open("POST", "/api/user"); 
            }else{
             ajaxRequest.open("PUT", "/api/user/"+id); 
            }
          

            /*== Add the content type in the header===============*/
            ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            ajaxRequest.send(parameter);
            }


function ajaxResponse() {

                 if(ajaxRequest.readyState != 4){
                    
                    console.log("Reqest not complete")

                 }else if(ajaxRequest.status == 200){
                       
                       alert("Profile Saved")
                    
                 }else{

                     alert("its in error, please try again");
                 }


          }

}